// Override Settings
var bcSfSearchSettings = {
    search: {
        //suggestionMode: 'test',
        suggestionPosition: 'right'
    }
};

// Customize style of Suggestion box
BCSfFilter.prototype.customizeSuggestion = function(suggestionElement, searchElement, searchBoxId) {
    if (jQ(searchBoxId).parent().hasClass('header-bar__search-form')) {
      	suggestionElement.outerWidth(300);
	}
};