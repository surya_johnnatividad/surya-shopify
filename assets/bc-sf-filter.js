// Override Settings
var bcSfFilterSettings = {
    general: {
        limit: bcSfFilterConfig.custom.products_per_page,
        // Optional
        loadProductFirst: true,
        showPlaceholderProductList: true,
        placeholderProductPerRow: 4,
        placeholderProductGridItemClass: 'grid__item wide--one-fifth large--one-quarter medium-down--one-half'
    }
};

var counter = 1;

// Build Product Grid Item
BCSfFilter.prototype.buildProductGridItem = function(data, index) {
    /*** Prepare data ***/
    var images = data.images_info;
     // Displaying price base on the policy of Shopify, have to multiple by 100
    var soldOut = !data.available; // Check a product is out of stock
    var onSale = data.compare_at_price_min > data.price_min; // Check a product is on sale
    var priceVaries = data.price_min != data.price_max; // Check a product has many prices
    // Get First Variant (selected_or_first_available_variant)
    var firstVariant = data['variants'][0];
    if (getParam('variant') !== null && getParam('variant') != '') {
        var paramVariant = data.variants.filter(function(e) { return e.id == getParam('variant'); });
        if (typeof paramVariant[0] !== 'undefined') firstVariant = paramVariant[0];
    } else {
        for (var i = 0; i < data['variants'].length; i++) {
            if (data['variants'][i].available) {
                firstVariant = data['variants'][i];
                break;
            }
        }
    }
    /*** End Prepare data ***/

    if (index == 1) counter = 1;

    // Get Template
    var itemHtml = bcSfFilterTemplate.productGridItemHtml;

    // Add soldOut class
    var itemSoldOutClass = soldOut ? bcSfFilterTemplate.soldOutClass : '';
    itemHtml = itemHtml.replace(/{{itemSoldOutClass}}/g, itemSoldOutClass);

    // Add onSale class
    var itemSaleClass = onSale ? bcSfFilterTemplate.saleClass : '';
    itemHtml = itemHtml.replace(/{{itemSaleClass}}/g, itemSaleClass);

    var textCenterClass = bcSfFilterConfig.custom.center_grid_link ? bcSfFilterTemplate.textCenterClass: '';
    itemHtml = itemHtml.replace(/{{textCenterClass}}/g, textCenterClass);

    var imageSoldOutClass = bcSfFilterConfig.custom.show_sold_out_circle ? bcSfFilterTemplate.imageSoldOutClass: '';
    itemHtml = itemHtml.replace(/{{imageSoldOutClass}}/g, imageSoldOutClass);

    // Add onSale label
    var itemSaleLabelHtml = '';
    if (onSale && bcSfFilterConfig.custom.show_sale_circle) {
        itemSaleLabelHtml = '<span class="badge badge--sale">';
        if (bcSfFilterConfig.label.sale.length > 7) {
            itemSaleLabelHtml += '<span class="badge__text badge__text--small">';
        } else {
            itemSaleLabelHtml += '<span class="badge__text">';
        }
        itemSaleLabelHtml += bcSfFilterConfig.label.sale + '</span></span>';
    }
    itemHtml = itemHtml.replace(/{{itemSaleLabel}}/g, itemSaleLabelHtml);

    // Add soldOut label
    var itemSoldOutLabeHtml = '';
    if (soldOut && bcSfFilterConfig.custom.show_sold_out_circle) {
        itemSoldOutLabeHtml = '<span class="badge badge--sold-out">';
        if (bcSfFilterConfig.label.sold_out.length > 9) {
            itemSoldOutLabeHtml += '<span class="badge__text badge__text--small">';
        } else {
            itemSoldOutLabeHtml += '<span class="badge__text">';
        }
        itemSoldOutLabeHtml += bcSfFilterConfig.label.sold_out + '</span></span>';
    }
    itemHtml = itemHtml.replace(/{{itemSoldOutLabel}}/g, itemSoldOutLabeHtml);

    //Add Review
    var itemReviews = '<div class="start-rating">' +
    '<div class="grid-view-item__reviews"><div class="yotpo bottomLine" data-appkey="twfIQLv1D0ukqcZdXQFKcTtc8xivhPD1IIQ7vPJ9" data-domain="{{itemDomain}}" data-product-id="{{itemId}}" data-product-models="{{itemId}}" data-name="{{itemTitle}}" data-url="{{itemUrl}}" data-image-url="{{itemThumbUrl}}" data-description="{{itemDescription}}" data-bread-crumbs="{{itemTags}}"> </div></div>' +
    '</div>';

    itemHtml = itemHtml.replace(/{{itemReview}}/g, itemReviews);


    // Add Image style
    var imageStyle = '';
    var itemImagesHtml = '';
    if (data.title != '') {
        if (images.length > 0) {
            var img_id = 'ProductImage-' + images[0]['id'];
            var wrapper_id = 'ProductImageWrapper-' + images[0]['id'] + '';

            var width = bcSfFilterConfig.custom.product_width;
            var height = 480;
            var aspect_ratio = images[0]['width'] / images[0]['height'];

            var max_width = height * aspect_ratio;
            if (images[0]['height'] < height) {
                var max_height = images[0]['height'];
                max_width = images[0]['width'];
            } else {
                var max_height = width / aspect_ratio;
                if (images[0]['width'] < width) {
                    var max_height = images[0]['height'];
                    max_width = images[0]['width'];
                } else {
                    max_width = width;
                }
            }

            imageStyle = '<style>' +
                            '#' + img_id + ' {' +
                                'max-width: ' + max_width + 'px;' +
                                'max-height: ' + max_height + 'px;' +
                            '}' +
                            '#' + wrapper_id + ' {' +
                                'max-width: ' + max_width + 'px;' +
                            '}' +
                        '</style>';

            var img_url = this.optimizeImage(images[0]['src'], '{width}x');
            itemImagesHtml += '<div id="' + wrapper_id + '" class="product__img-wrapper supports-js">';
          itemImagesHtml += '<div style="height:270px">' +//padding-top:' + ( 1 / aspect_ratio * 100) + '%;
                                '<img id="' + img_id + '" ' +
                                      'class="product__img lazyload" ' +
                                      'src="' + this.optimizeImage(images[0]['src'], '300x300') + '" ' +
                                      'data-src="' + img_url + '" ' +
                                      'data-widths="[150, 220, 360, 470, 600, 750, 940, 1080, 1296, 1512, 1728, 2048]" ' +
                                      'data-aspectratio="' + aspect_ratio + '" ' +
                                      'data-sizes="auto" '+
                                      'alt="{{itemTitle}}">' +
                              '</div>';
            itemImagesHtml += '</div>';
        } else {
            itemImagesHtml += '<img src="' + bcSfFilterConfig.general.no_image_url + '" alt="{{itemTitle}}" class="product__img">';
        }
    } else {
        if (counter == 7) counter = 1;
        if (bcSfFilterConfig.custom.hasOwnProperty('placeholder_svg_tag_' + counter)) {
            itemImagesHtml += bcSfFilterConfig['custom']['placeholder_svg_tag_' + counter];
        }
        counter++;
    }
    itemHtml = itemHtml.replace(/{{imageStyle}}/g, imageStyle);
    itemHtml = itemHtml.replace(/{{itemImages}}/g, itemImagesHtml);

    // Add Vendor
    var itemVendorHtml = bcSfFilterConfig.custom.vendor_enable ? bcSfFilterTemplate.vendorHtml : '';
    itemHtml = itemHtml.replace(/{{itemVendor}}/g, itemVendorHtml);

    // Build Domain
    itemHtml = itemHtml.replace(/{{itemDomain}}/g, this.escape(bcSfFilterConfig.shop.domain));
    
    // Build Description
    var itemDescription = data.description;
    itemDescription = itemDescription.substr(0, itemDescription.indexOf('##highlights##'));
    itemHtml = itemHtml.replace(/{{itemDescription}}/g, itemDescription);
    
    // Build Tags
    itemHtml = itemHtml.replace(/{{itemTags}}/g, this.escape(data.tags.join(';')));    

    // Add Price
    var itemPriceHtml = '';
    if (data.title != '')  {
        itemPriceHtml += '<p class="grid-link__meta">';
        if (onSale) {
            itemPriceHtml += '<span class="visually-hidden">' + bcSfFilterConfig.label.regular_price + '</span>';
            itemPriceHtml += '<s class="grid-link__sale_price">' + this.formatMoney(data.compare_at_price_min, this.moneyFormat) + '</s> ';
        }
        if (priceVaries) {
            itemPriceHtml += (bcSfFilterConfig.label.from_price).replace(/{{ price }}/g, this.formatMoney(data.price_min, this.moneyFormat));
        } else {
            itemPriceHtml += onSale ? '<span class="visually-hidden">' + bcSfFilterConfig.label.sale_price + '</span>' : '<span class="visually-hidden">' + bcSfFilterConfig.label.regular_price + '</span>';
             itemPriceHtml += this.formatMoney(data.price_min, this.moneyFormat); 
        }
        itemPriceHtml += '</p>';
    }
    itemHtml = itemHtml.replace(/{{itemPrice}}/g, itemPriceHtml);

    // Add data json
    itemHtml = itemHtml.replace(/{{itemJson}}/g, JSON.stringify(data.json));

    // Add main attribute
    itemHtml = itemHtml.replace(/{{itemId}}/g, data.id);
    itemHtml = itemHtml.replace(/{{itemTitle}}/g, data.title);
    itemHtml = itemHtml.replace(/{{itemVendorLabel}}/g, data.vendor);
    itemHtml = itemHtml.replace(/{{itemUrl}}/g, this.buildProductItemUrl(data));

    return itemHtml;
};

// Build Pagination
BCSfFilter.prototype.buildPagination = function(totalProduct) {
    // Get page info
    var currentPage = parseInt(this.queryParams.page);
    var totalPage = Math.ceil(totalProduct / this.queryParams.limit);

    // If it has only one page, clear Pagination
    if (totalPage == 1) {
        jQ(this.selector.pagination).html('');
        return false;
    }

    if (this.getSettingValue('general.paginationType') == 'default') {
        var paginationHtml = bcSfFilterTemplate.paginateHtml;

        // Build Previous
        var previousHtml = (currentPage > 1) ? bcSfFilterTemplate.previousActiveHtml : bcSfFilterTemplate.previousDisabledHtml;
        previousHtml = previousHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage - 1));
        paginationHtml = paginationHtml.replace(/{{previous}}/g, previousHtml);

        // Build Next
        var nextHtml = (currentPage < totalPage) ? bcSfFilterTemplate.nextActiveHtml :  bcSfFilterTemplate.nextDisabledHtml;
        nextHtml = nextHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, currentPage + 1));
        paginationHtml = paginationHtml.replace(/{{next}}/g, nextHtml);

        // Create page items array
        var beforeCurrentPageArr = [];
        for (var iBefore = currentPage - 1; iBefore > currentPage - 3 && iBefore > 0; iBefore--) {
            beforeCurrentPageArr.unshift(iBefore);
        }
        if (currentPage - 4 > 0) {
            beforeCurrentPageArr.unshift('...');
        }
        if (currentPage - 4 >= 0) {
            beforeCurrentPageArr.unshift(1);
        }
        beforeCurrentPageArr.push(currentPage);

        var afterCurrentPageArr = [];
        for (var iAfter = currentPage + 1; iAfter < currentPage + 3 && iAfter <= totalPage; iAfter++) {
            afterCurrentPageArr.push(iAfter);
        }
        if (currentPage + 3 < totalPage) {
            afterCurrentPageArr.push('...');
        }
        if (currentPage + 3 <= totalPage) {
            afterCurrentPageArr.push(totalPage);
        }

        // Build page items
        var pageItemsHtml = '';
        var pageArr = beforeCurrentPageArr.concat(afterCurrentPageArr);
        for (var iPage = 0; iPage < pageArr.length; iPage++) {
            if (pageArr[iPage] == '...') {
                pageItemsHtml += bcSfFilterTemplate.pageItemRemainHtml;
            } else {
                pageItemsHtml += (pageArr[iPage] == currentPage) ? bcSfFilterTemplate.pageItemSelectedHtml : bcSfFilterTemplate.pageItemHtml;
            }
            pageItemsHtml = pageItemsHtml.replace(/{{itemTitle}}/g, pageArr[iPage]);
            pageItemsHtml = pageItemsHtml.replace(/{{itemUrl}}/g, this.buildToolbarLink('page', currentPage, pageArr[iPage]));
        }
        paginationHtml = paginationHtml.replace(/{{pageItems}}/g, pageItemsHtml);

        jQ(this.selector.pagination).html(paginationHtml);
    }
};

// Build Sorting
BCSfFilter.prototype.buildFilterSorting = function() {
    if (bcSfFilterTemplate.hasOwnProperty('sortingHtml')) {
        jQ(this.selector.topSorting).html('');

        var sortingArr = this.getSortingList();
        if (sortingArr) {
            // Build content
            var sortingItemsHtml = '';
            for (var k in sortingArr) {
                sortingItemsHtml += '<option value="' + k +'">' + sortingArr[k] + '</option>';
            }
            var html = bcSfFilterTemplate.sortingHtml.replace(/{{sortingItems}}/g, sortingItemsHtml);
            jQ(this.selector.topSorting).html(html);

            // Set current value
            jQ(this.selector.topSorting + ' select').val(this.queryParams.sort);
        }
    }
};

// Build Breadcrumb
BCSfFilter.prototype.buildBreadcrumb = function(colData, apiData) {
    if (typeof colData !== 'undefined' && colData.hasOwnProperty('collection')) {
        var colInfo = colData.collection;
        var breadcrumbHtml = '<a href="/" title="' + bcSfFilterConfig.label.breadcrumb_home_link + '">' + bcSfFilterConfig.label.breadcrumb_home + '</a>';
        breadcrumbHtml += ' <span aria-hidden="true" class="breadcrumb__sep">&rsaquo;</span>';
        if (bcSfFilterConfig.general.current_tags !== null) {
            var currentTags = bcSfFilterConfig.general.current_tags;
            breadcrumbHtml += ' <a href="/collections/' + colInfo.handle + '">' + colInfo.title + '</a>';
            breadcrumbHtml += ' <span aria-hidden="true" class="breadcrumb__sep">&rsaquo;</span>';
            breadcrumbHtml += ' <span>' + currentTags.join(' + ') + '</span>';
        } else {
            breadcrumbHtml += ' <span>' + colInfo.title + '</span>';
        }
        jQ('.breadcrumb').html(breadcrumbHtml);
    }
};

// Add additional feature for product list, used commonly in customizing product list
BCSfFilter.prototype.buildExtrasProductList = function(data, eventType) {
    /* start-initialize-bc-al */
    var self = this;
    var alEnable = true;
    if(self.getSettingValue('actionlist.qvEnable') != '' || self.getSettingValue('actionlist.atcEnable') != ''){
      alEnable = self.getSettingValue('actionlist.qvEnable') || self.getSettingValue('actionlist.atcEnable');
    }
    if (alEnable === true && typeof BCActionList !== 'undefined') {
        if (typeof bcActionList === 'undefined') {
            bcActionList = new BCActionList();
        }else{
          if (typeof bcAlParams !== 'undefined' && typeof bcSfFilterParams !== 'undefined') {
              bcActionList.initFlag = false;
              bcActionList.alInit(bcSfFilterParams, bcAlParams);
          } else {
              bcActionList.alInit();
          }
        }
    }
    /* end-initialize-bc-al */};

// Build additional elements
BCSfFilter.prototype.buildAdditionalElements = function(data, eventType) {
    if (typeof Yotpo !== 'undefined') {
        var api = new Yotpo.API(yotpo);
        api.refreshWidgets();
    }    
}


// Build Default layout
BCSfFilter.prototype.buildDefaultElements = function () {
    var isiOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream,
        isSafari = /Safari/.test(navigator.userAgent),
        isBackButton = window.performance && window.performance.navigation && 2 == window.performance.navigation.type;
    if (!(isiOS && isSafari && isBackButton)) {
        var self = this,
            url = window.location.href.split("?")[0],
            searchQuery = self.isSearchPage() && self.queryParams.hasOwnProperty("q") ? "&q=" + self.queryParams.q : "";
        window.location.replace(url + "?view=bc-original" + searchQuery);
    }
};

function customizeJsonProductData(data) {
    for (var i = 0; i < data.variants.length; i++) {
        var variant = data.variants[i];
        var featureImage = data.images.filter(function (e) {
            return e.src == variant.image;
        });
        if (featureImage.length > 0) {
            variant.featured_image = {
                id: featureImage[0]["id"],
                product_id: data.id,
                position: featureImage[0]["position"],
                created_at: "",
                updated_at: "",
                alt: null,
                width: featureImage[0]["width"],
                height: featureImage[0]["height"],
                src: featureImage[0]["src"],
                variant_ids: [variant.id],
            };
        } else {
            variant.featured_image = "";
        }
    }
    var self = bcsffilter;
    var itemJson = {
        id: data.id,
        title: data.title,
        handle: data.handle,
        vendor: data.vendor,
        variants: data.variants,
        url: self.buildProductItemUrl(data),
        options_with_values: data.options_with_values,
        images: data.images,
        images_info: data.images_info,
        available: data.available,
        price_min: data.price_min,
        price_max: data.price_max,
        compare_at_price_min: data.compare_at_price_min,
        compare_at_price_max: data.compare_at_price_max,
    };
    return itemJson;
}
BCSfFilter.prototype.prepareProductData = function (data) {
    var countData = data.length;
    for (var k = 0; k < countData; k++) {
        data[k]["images"] = data[k]["images_info"];
        if (data[k]["images"].length > 0) {
            data[k]["featured_image"] = data[k]["images"][0];
        } else {
            data[k]["featured_image"] = { src: bcSfFilterConfig.general.no_image_url, width: "", height: "", aspect_ratio: 0 };
        }
        data[k]["url"] = "/products/" + data[k].handle;
        var optionsArr = [];
        var countOptionsWithValues = data[k]["options_with_values"].length;
        for (var i = 0; i < countOptionsWithValues; i++) {
            optionsArr.push(data[k]["options_with_values"][i]["name"]);
        }
        data[k]["options"] = optionsArr;
        if (typeof bcSfFilterConfig.general.currencies != "undefined" && bcSfFilterConfig.general.currencies.length > 1) {
            var currentCurrency = bcSfFilterConfig.general.current_currency.toLowerCase().trim();
            function updateMultiCurrencyPrice(oldPrice, newPrice) {
                if (typeof newPrice != "undefined") {
                    return newPrice;
                }
                return oldPrice;
            }
            data[k].price_min = updateMultiCurrencyPrice(data[k].price_min, data[k]["price_min_" + currentCurrency]);
            data[k].price_max = updateMultiCurrencyPrice(data[k].price_max, data[k]["price_max_" + currentCurrency]);
            data[k].compare_at_price_min = updateMultiCurrencyPrice(data[k].compare_at_price_min, data[k]["compare_at_price_min_" + currentCurrency]);
            data[k].compare_at_price_max = updateMultiCurrencyPrice(data[k].compare_at_price_max, data[k]["compare_at_price_max_" + currentCurrency]);
        }
        (data[k]["price_min"] *= 100), (data[k]["price_max"] *= 100), (data[k]["compare_at_price_min"] *= 100), (data[k]["compare_at_price_max"] *= 100);
        data[k]["price"] = data[k]["price_min"];
        data[k]["compare_at_price"] = data[k]["compare_at_price_min"];
        data[k]["price_varies"] = data[k]["price_min"] != data[k]["price_max"];
        var firstVariant = data[k]["variants"][0];
        if (getParam("variant") !== null && getParam("variant") != "") {
            var paramVariant = data[k]["variants"].filter(function (e) {
                return e.id == getParam("variant");
            });
            if (typeof paramVariant[0] !== "undefined") firstVariant = paramVariant[0];
        } else {
            var countVariants = data[k]["variants"].length;
            for (var i = 0; i < countVariants; i++) {
                if (data[k]["variants"][i].available) {
                    firstVariant = data[k]["variants"][i];
                    break;
                }
            }
        }
        data[k]["selected_or_first_available_variant"] = firstVariant;
        var countVariants = data[k]["variants"].length;
        for (var i = 0; i < countVariants; i++) {
            var variantOptionArr = [];
            var count = 1;
            var variant = data[k]["variants"][i];
            var variantOptions = variant["merged_options"];
            if (Array.isArray(variantOptions)) {
                var countVariantOptions = variantOptions.length;
                for (var j = 0; j < countVariantOptions; j++) {
                    var temp = variantOptions[j].split(":");
                    data[k]["variants"][i]["option" + (parseInt(j) + 1)] = temp[1];
                    data[k]["variants"][i]["option_" + temp[0]] = temp[1];
                    variantOptionArr.push(temp[1]);
                }
                data[k]["variants"][i]["options"] = variantOptionArr;
            }
            data[k]["variants"][i]["compare_at_price"] = parseFloat(data[k]["variants"][i]["compare_at_price"]) * 100;
            data[k]["variants"][i]["price"] = parseFloat(data[k]["variants"][i]["price"]) * 100;
        }
        data[k]["description"] = data[k]["content"] = data[k]["body_html"];
        if (data[k].hasOwnProperty("original_tags") && data[k]["original_tags"].length > 0) {
            data[k]["tags"] = data[k]["original_tags"].slice(0);
        }
        data[k]["json"] = customizeJsonProductData(data[k]);
    }
    return data;
};

BCSfFilter.prototype.getFilterData = function (eventType, errorCount) {
    function BCSend(eventType, errorCount) {
        var self = bcsffilter;
        var errorCount = typeof errorCount !== "undefined" ? errorCount : 0;
        self.showLoading();
        if (typeof self.buildPlaceholderProductList == "function") {
            self.buildPlaceholderProductList(eventType);
        }
        self.beforeGetFilterData(eventType);
        self.prepareRequestParams(eventType);
        self.queryParams["callback"] = "BCSfFilterCallback";
        self.queryParams["event_type"] = eventType;
        var url = self.isSearchPage() ? self.getApiUrl("search") : self.getApiUrl("filter");
        var script = document.createElement("script");
        script.type = "text/javascript";
        var timestamp = new Date().getTime();
        script.src = url + "?t=" + timestamp + "&" + jQ.param(self.queryParams);
        script.id = "bc-sf-filter-script";
        script.async = true;
        var resendAPITimer, resendAPIDuration;
        resendAPIDuration = 2e3;
        script.addEventListener("error", function (e) {
            if (typeof document.getElementById(script.id).remove == "function") {
                document.getElementById(script.id).remove();
            } else {
                document.getElementById(script.id).outerHTML = "";
            }
            if (errorCount < 3) {
                errorCount++;
                if (resendAPITimer) {
                    clearTimeout(resendAPITimer);
                }
                resendAPITimer = setTimeout(self.getFilterData("resend", errorCount), resendAPIDuration);
            } else {
                self.buildDefaultElements(eventType);
            }
        });
        document.getElementsByTagName("head")[0].appendChild(script);
        script.onload = function () {
            if (typeof document.getElementById(script.id).remove == "function") {
                document.getElementById(script.id).remove();
            } else {
                document.getElementById(script.id).outerHTML = "";
            }
        };
    }
    this.requestFilter(BCSend, eventType, errorCount);
};
BCSfFilter.prototype.requestFilter = function (sendFunc, eventType, errorCount) {
    sendFunc(eventType, errorCount);
};
